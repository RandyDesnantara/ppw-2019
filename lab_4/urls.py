from django.urls import include, path
from .views import index, portfolio

urlpatterns = [
    path('', index, name='index'),
    path('portfolio/', portfolio, name='portfolio'),
]
