from django.shortcuts import render
from datetime import date

# Create your views here.

def index(request):
    response = {}
    return render(request, 'index.html', response)

def portfolio(request):
    response = {}
    return render(request, 'portfolio.html', response)

def current_date():
    return date.today()